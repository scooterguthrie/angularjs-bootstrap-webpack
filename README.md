# AngularJS, Bootstrap, and Webpack #
This is a kickoff workflow with AngularJS, Bootstrap, and Webpack

## Installation
* [Node.js](https://nodejs.org/en/)
* run ``` $ npm install ```

## NPM Scripts
* Build ``` $ npm run start ```
* Build with Watch``` $ npm run watch```
