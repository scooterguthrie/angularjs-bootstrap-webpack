angular.module('app').config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
        .otherwise('/home');

    $stateProvider

        // Templates
        .state('main', {
            url: '',
            abstract: true,
            views: {
                '': {
                    templateUrl: 'ui/_shared/templates/main/main.template.html',
                    controller: 'MainCtrl'
                },
                "header@main": {
                    templateUrl: 'ui/_shared/main-navbar/main-navbar.template.html'
                },
                "footer@main": {
                    templateUrl: 'ui/_shared/footer/footer.template.html',
                    controller: 'FooterCtrl'
                }
            }
        })

        // Home
        .state('main.home', {
            url: '/home',
            views: {
                "main-content@main": {
                    templateUrl: 'ui/home/home.template.html'
                }
            }
        })

        // About
        .state('main.about', {
            url: '/about',
            views: {
                "main-content@main": {
                    templateUrl: 'ui/about/about.template.html'
                }
            }
        })

        // Contact
        .state('main.contact', {
            url: '/contact',
            views: {
                "main-content@main": {
                    templateUrl: 'ui/contact/contact.template.html'
                }
            }
        });
});