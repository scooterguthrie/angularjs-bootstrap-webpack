// Templates
require("./templates/main/main.template.html");
require("./templates/main/main.controller.js");

// Main Nav
require("./main-navbar/main-navbar.template.html");
require("./main-navbar-links/main-navbar-links.template.html");
require("./main-navbar-links/main-navbar-links.component.js");


// Footer
require("./footer/footer.template.html");
require("./footer/footer.controller.js");