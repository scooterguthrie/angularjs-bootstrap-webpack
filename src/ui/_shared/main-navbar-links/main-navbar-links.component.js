// function MainNavbarLinksCtrl() {
// }

// angular.module('app').component('mainNavbarLinks', {
//   templateUrl: "ui/_shared/main-navbar-links/main-navbar-links.template.html",
//   controller: MainNavbarLinksCtrl
// });

angular.module('app').directive('mainNavbarLinks', function() {
  return {
    templateUrl: "ui/_shared/main-navbar-links/main-navbar-links.template.html"
  };
});