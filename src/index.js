// Application Vendors Dependencies
// ****************************************************
import 'angular/angular.min.js';
import 'angular-animate/angular-animate.min.js';
//import 'angular-sanitize';
import 'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js';
import 'angular-ui-router/release/angular-ui-router.min.js';
//import uiSelect from 'ui-select';


// Angular Module
// ****************************************************
angular.module('app', [
    'ngAnimate',
    'ui.bootstrap',
    'ui.router'
]);


// Global Application Files
// ****************************************************
require("./scripts/app.js");
require("./styles/app.scss");


// Shared
// ****************************************************
require("./ui/_shared");


// Components
// ****************************************************
// Home
require("./ui/home");
// About
require("./ui/about");
// Contact
require("./ui/contact");


// Images
// ****************************************************
require('./images');